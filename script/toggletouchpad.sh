#!/bin/sh
# toggle-touchpad script
#
# Toggles the touchpad on and off. Bind this script to a key to turn on and off
# your touchpad/trackpad.

# This is the version for Asus Rog G14 on AcroLinux i3
# ##########################################################
# DovendyrE
#
if [ $(xinput list-props "ELAN1201:00 04F3:3098 Touchpad" | grep 'Device Enabled' | gawk -F: '{print $2}' ) -eq 0 ]; then
xinput enable "ELAN1201:00 04F3:3098 Touchpad"
else
xinput disable "ELAN1201:00 04F3:3098 Touchpad"
fi
