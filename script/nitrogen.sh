#!/bin/sh

#### Edited an existing script. Just changes the background when the script is launched!!!! ####

## Nitrogenium is a wallpaper randomizer for Openbox based on Nitrogen
## this script was inspired by but is *NOT* based on Variety by Peter Levi

## requires: nitrogen, imagemagick, notify-send

# some settings
CONFIG=/home/$USER/.config

# get path of current wallpaper from bg-saved.cfg
CURRENT=`grep -Po "(?<=file=).*" "$CONFIG/nitrogen/bg-saved.cfg"`
#read the wallpaper directories from nitrogen.cfg
FILES=`grep -Po "(?<=dirs=).*" "$CONFIG/nitrogen/nitrogen.cfg" | sed "s/;/\/\* /g"`

    # pick a random wallpaper from these directories and make sure it's different from the current wallpaper
    NEW=`ls $FILES | grep -Ei ".*\.(png|jpe?g|bmp)" | shuf -n 1`
    while [ "$NEW" = "$CURRENT" ]; do
        NEW=`ls $FILES | grep -Ei ".*\.(png|jpe?g|bmp)" | shuf -n 1`
    done

    # set the wallpaper with nitrogen
    nitrogen --set-zoom-fill $NEW && sed -i "s!^file=.*!file=$NEW!" $CONFIG/nitrogen/bg-saved.cfg
