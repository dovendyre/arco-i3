# Arco-i3

My ArcoLinux setup with i3 and i3blocks.

Feel free to tailor this for you own needs!

CAUTION! Use at own risk!

## Installation

* This script assumes that you have Arcolinux with i3 desktop


```
git clone https://gitlab.com/dovendyre/arco-i3.git && sh arco-i3/doven_install.sh
```
