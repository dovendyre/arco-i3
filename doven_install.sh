#!/bin/sh

echo "You need to reboot the compute when the script are finnished..."

### Henter ut i hvilken mappe scriptet ligger
scriptdir=$(dirname $(readlink -f $0))


## Make a variable with all programs to install
programs=""
programs="${programs} seahorse"
programs="${programs} timeshift"
programs="${programs} light"
programs="${programs} sysstat"
programs="${programs} acpi"
programs="${programs} nitrogen"
programs="${programs} arcolinux-meta-fun"
programs="${programs} qt5-multimedia"
programs="${programs} linux-zen"
programs="${programs} linux-zen-headers"

### Installing som extra software and the zen kernel
for program in ${programs}; do
        sudo pacman --needed --noconfirm -S ${program}
done

### Removes the system picom package, new one will be installed from AUR
    sudo pacman -R picom


## Make a variable with all aur programs to install
aurprograms=""
aurprograms="${aurprograms} picom-ibhagwan-git"
aurprograms="${aurprograms} sddm-theme-aerial-git"

### Installing som extra software from the AUR repo
for aurprogram in ${aurprograms}; do
    if pacman -Qs ${aurprogram} > /dev/null ; then
        echo "Package ${aurprogram} installed, skipping"
    else
        sudo paru --noconfirm -S ${aurprogram}
    fi
done


[ ! -f /usr/share/sddm/themes/aerial/theme.conf.user ] &&  sudo cp -f /usr/share/sddm/themes/aerial/theme.conf /usr/share/sddm/themes/aerial/theme.conf.user

## Changing look and feel
[ -f ~/.config/gtk-3.0/settings.ini ] && mv ~/.config/gtk-3.0/settings.ini ~/.config/gtk-3.0/settings.ini_org
cp -f $scriptdir/config/settings.ini ~/.config/gtk-3.0/
sed -i "/Inherits=/c\Inherits=oreo_spark_orange_cursors" ~/.icons/default/index.theme

## If picom or i3 config exists, backup them
[ -f ~/.config/i3/config ] && [ ! -f ~/.config/i3/config_org ] && mv ~/.config/i3/config ~/.config/i3/config_org
[ -f ~/.config/i3/picom.conf ] && [ ! -f ~/.config/i3/picom.conf_org ] && mv ~/.config/i3/picom.conf ~/.config/i3/picom.conf_org

## Backups existing scripts, i3blocks and arcologout
[ -d ~/.config/i3blocks ] && [ ! -d ~/.config/i3blocks_org ] && mv ~/.config/i3blocks ~/.config/i3blocks_org
[ -d ~/.config/i3/scripts ] && [ ! -d ~/.config/i3/scripts_org ] && mv ~/.config/i3/scripts ~/.config/i3/scripts_org
[ -d ~/.config/arcologout ] && [ ! -d ~/.config/arcologout_org ] && mv ~/.config/arcologout ~/.config/arcologout_org

## "Ads new picom and i3 config"
cp -f -R $scriptdir/i3/* ~/.config/i3/

## Ads i3blocks config
cp -f -R $scriptdir/i3blocks ~/.config/

## Ads new arcologout config
cp -f -R $scriptdir/arcologout ~/.config/

## Checks if rofi exists and backups the existing
[ -d ~/.config/rofi ] && [ ! -d ~/.config/rofi_org ] && mv ~/.config/rofi ~/.config/rofi_org

## Ads the new rofi config
cp -f -R $scriptdir/rofi ~/.config/

## Ads some new script folder and put some scripts in it
[ ! -d ~/.config/dovenscripts ] && mkdir ~/.config/dovenscripts
cp -f -R $scriptdir/script/* ~/.config/dovenscripts/

## Change text to correct keybinding on arcologout
[ -f /usr/share/arcologout/GUI.py ] && sudo sed -i 's/(L)/(O)/g' /usr/share/arcologout/GUI.py
[ -f /usr/share/arcologout/GUI.py ] && sudo sed -i 's/(K)/(L)/g' /usr/share/arcologout/GUI.py

## Backup the original termite config
[ -f ~/.config/termite/config ] && [ ! -f ~/.config/termite/config_org ] && mv ~/.config/termite/config ~/.config/termite/config_org

## Updateing the termite config
cp -f -R $scriptdir/termite/* ~/.config/termite/

## Add the new themes icons and pointers
sudo cp -f -R $scriptdir/icons/* /usr/share/icons/
sudo cp -f -R $scriptdir/themes/* /usr/share/themes/
sudo cp -f -R $scriptdir/fonts/* /usr/share/fonts/

## Adding wallpapers to home dir
[ ! -d ~/.config/wallpapers ] && mkdir ~/.config/wallpapers
cp -f -R /$scriptdir/wallpapers/* ~/.config/wallpapers/
sed -i "/dirs=/c\dirs=/home/$USER/.config/wallpapers;" ~/.config/nitrogen/nitrogen.cfg

## Sets a new random background
sh ~/.config/dovenscripts/nitrogen.sh

## Installes the DKMS nvidia driver?
read -p "Having a NVIDIA graphics card? (y/n)" -n 1 -r 
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo pacman --needed -S --noconfirm nvidia-dkms

    ## Enable overclock?
    read -p "Install GreenWithEnvy GPU overclock software? (y/n)" -n 1 -r 
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        sudo pacman --needed -S --noconfirm gwe
        sudo nvidia-xconfig --cool-bits=8
        sed -i "/#exec --no-startup-id gwe/c\exec --no-startup-id gwe --hide-window" ~/.config/i3/config
    fi
fi

## Install corsair software?
read -p "Having a Corsair keybord or mouse? (y/n)" -n 1 -r 
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo pacman --needed -S --noconfirm ckb-next-git
    sed -i "/#exec --no-startup-id ckb-next/c\exec --no-startup-id ckb-next -b" ~/.config/i3/config
fi

# Fixing some spcial config for my ASUS laptop
read -p "Using a Asus Rog G14 laptop? (y/n)" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    if pacman -Qs asusctl > /dev/null ; then    
        echo "Package asusctl installed, skipping"    
    else    
        paru -S --noconfirm asusctl
    fi 
    sed -i "/#bindsym XF86TouchpadToggle/c\bindsym XF86TouchpadToggle exec ~/.config/dovenscripts/toggletouchpad.sh # toggle touchpad" ~/.config/i3/config
    sed -i "/#bindsym XF86KbdBrightnessUp/c\bindsym XF86KbdBrightnessUp exec --no-startup-id asusctl -n # increase keyboard light" ~/.config/i3/config
    sed -i "/#bindsym XF86KbdBrightnessDown/c\bindsym XF86KbdBrightnessDown exec --no-startup-id asusctl -p # decrease keyboard light" ~/.config/i3/config
fi

## Install nextcloud sync client?
read -p "Install nextcloud desktop synk tool? (y/n)" -n 1 -r 
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo pacman --needed -S --noconfirm nextcloud-client
    sed -i "/#exec --no-startup-id nextcloud/c\exec --no-startup-id nextcloud" ~/.config/i3/config
fi

# Offer to install my awsome-zsh-with-neovim 
read -p "Would you like to install my awsome-zsh-with-neovim setup? (y/n)" -n 1 -r 
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    mkdir $scriptdir/tmp && git clone https://gitlab.com/dovendyre/awsome-zsh-with-neovim.git $scriptdir/tmp && sh $scriptdir/tmp/doven_install.sh && rm -rf $scriptdir/tmp
fi


echo " D O N E ! "
echo " "
echo " Add your favourute wallpapers to the ~/.config/wallpapers directory. Will be randomized at login."
echo " "
echo " If you have a multi monitor setup run lxrandr and set ut your monitors and save "
echo " Then run the command: cat ~/.config/autostart/lxrandr-autostart.desktop |grep Exec= |sed 's/Exec=/exec --no-startup-id /g' >> ~/.config/i3/config"
echo " "
echo " You can now reboot.. "
